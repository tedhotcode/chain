/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const HopContract = require('./lib/hop-contract');

module.exports.HopContract = HopContract;
module.exports.contracts = [ HopContract ];
