/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const shim = require('fabric-shim');


const crypto = require('crypto');
const myCollectionName = 'CollectionOne';

class HospitalContract extends Contract {

    async hospitalExists(ctx, phn) {
        const buffer = await ctx.stub.getState(phn);
        return (!!buffer && buffer.length > 0);
    }

    async createHospital(ctx, phn, name,reason) {
        let logger = shim.newlogger ('chaincode -->');
        let CID = new shim.ClientIdentity(ctx.stub)
        let mspID = CID.getMSPID();
        logger.info('MSPID: ' + mspID);

        if(mspID == 'PatientMSP'){
            const exists = await this.hospitalExists(ctx,phn);
            if (exists) {
                throw new Error('A patient with phn number: ${phn} already exits');
            } 
            const asset = {
                name,
                reason
            };
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(phn,buffer);

            let addPatEvent = {Type: 'pat creation', Reason: reason};
            await ctx.stub.setEvent('addPatEvent', Buffer.from(JSON.stringify(addpatEvent)));
        }
        else {
            logger.info('Users under the following MSP : '+
                              mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
        }
        
    }

    async readHospital(ctx, phn) {
        const exists = await this.hospitalExists(ctx, phn);
        if (!exists) {
            throw new Error(`The hospital ${phn} does not exist`);
        }
        const buffer = await ctx.stub.getState(phn);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    

    async deleteHospital(ctx, phn) {
        let logger =  shim.newLogger('chaincode --> ');
        let CID =  new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : ' + mspID);

        if(mspID == 'PatientMSP'){
            const exists = await this.hospitalExists(ctx,phn);
        
        
        if (!exists) {
            throw new Error(`The hospital ${phn} does not exist`);
        }
        await ctx.stub.deleteState(phn);
    }
    else {
        logger.info('Users under the following MSP: '+ 
                    mspID + 'cannot perform this action');
        return('Users under the following MSP : '+ 
        mspID + 'cannot perform this action');
    }

}





async hospitalExists(ctx, hospitalId) {
    const buffer = await ctx.stub.getState(hospitalId);
    return (!!buffer && buffer.length > 0);
}
async createHospital(ctx, phn,ill,pres,time) {
    let logger = shim.newlogger ('chaincode -->');
    let CID = new shim.ClientIdentity(ctx.stub)
    let mspID = CID.getMSPID();
    logger.info('MSPID: ' + mspID)

    if(mspID == 'PatientMSP'){
        const exists = await this.hospitalExists(ctx,phn);
        if (exists) {
            throw new Error('A patient with phn number: ${phn} already exits');
        } 
        const asset = {
            ill,
            pres,
            time
        };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(phn,buffer);

        let addpatEvent = {Type: 'pat creation', Reason: reason};
        await ctx.stub.setEvent('addpatEvent', Buffer.from(JSON.stringify(addpatEvent)));
    }
    else {
        logger.info('Users under the following MSP : '+
                          mspID + 'cannot perform this action');
        return('Users under the following MSP : '+
        mspID + 'cannot perform this action');
    }
    
}

async readHospital(ctx, phn) {
    const exists = await this.hospitalExists(ctx, phn);
    if (!exists) {
        throw new Error(`The hospital ${phn} does not exist`);
    }
    const buffer = await ctx.stub.getState(phn);
    const asset = JSON.parse(buffer.toString());
    return asset;
}

}

module.exports = HospitalContract;
